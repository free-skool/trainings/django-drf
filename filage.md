# Django Filage

## Résumé

- TP / Notre projet cobaye
- Projets / travail sur les projets

### Journée 1

#### Matin

- initiation Django : création projet + app, premier modèle, admin, django shell, première vue et urls, deuxième modèle et relations / TP

#### Après-midi

- Projets / 1h : conception du modèle de donnée, implémentation d'un premier modèle
- Django : ajouter une app (debug toolbar), mon premier test, créer une vue qui renvoie du json, utilisation avec curl / TP
- Initiation DRF : ajouter DRF à Django, création API sur 1 modèle : ModelSerializer, première vue, urls de l'API, utilisation avec curl / TP

#### Attention

- include urls (ne pas oublier "'")
- django 3 : settings → 'bookshare.apps.BookshareConfig'
- view json classique : JsonResponse (safe=False)
- json.loads(data) en utilisant le serializer de Django

#### À faire chez vous

- créez votre application django et une première API
- si vous voulez aller plus loin avec django : tutoriel → parties 1, 2 et 3

### Journée 2

#### Matin

On passe sur le fil blanc

- Django
    - la clé des champs : Meta, plus de champs…
    - many to many : relation Aliment → Plat. Relation Question → Joueur
    - les querysets : faire des filtres, des recherches
    - django commands : importer les données depuis une base

#### Après-midi

- projets

#### À faire

- si applicable, ajoutez le champs geometrie à votre modèle
- créez votre API

### Journée 3

- Django : tests, many-to-many
- DRF : authentification basique
- GeoDjango

#### À faire

- installez un serveur pour votre application

### Journée 4

- Django : many-to-many, one-to-one, foreignkey / optimisation
- Django : vues fonctions et class based view
- Django : utiliser postgres
- DRF : filtres
- Projets / 3h sur les projets

## Projets / Exemples

Créer l'appli selon le fil rouge, pour réfléchir et expérimenter : ce sera un brouillon que vous pourrez jeter à la fin du cours, car vous devrez sûrement repenser des choses depuis zero pour construire votre vraie appli.

Champs communs : nom ou titre, description / résumé, date

* Appli mobile cartographique de partage de points d’intérêt
    * modèles : profil voyageur, point d'intérêt, commentaires
* Appli mobile sociale de photos pour des conseils de mode
    * modèles : profil photographe, photo, commentaires
* Appli sportive collaborative
    * modèles : profil sportif, sport, performances
* Appli partage de livres
    * modèles : profil utilisateur, livre, collection, prêt / transaction / échange
* Enstein battle 
    * modèles : profil joueur, questions, réponses

## Déroulé TP

### Jour1 : matin

- créer le projet
- créer l'app
- explorer les fichiers
- lancer le serveur
- initialiser le dépôt git (resource : https://www.gitignore.io/)

### La première vue

- créer une vue http "hello"
- déclarer une url dans l'app
- inclure les urls de l'app dans les urls du projet

### Les modèles

- configurer la base de donnée : laisser sqlite pour le moment
- lancer les migrations
- créer un modèle avec 2 champs : titre et booléen
- générer les migrations
- lancer les migrations
- créer un superuser
- se connecter à l'admin
- relier l'admin au modèle
- explorer avec django admin, créer des contenus
- explorer avec django shell
- créer un deuxième modèle

### Jour1 : après-midi

### Vues listes et élément

- créer une vue pour accéder à l'élément
- créer une vue liste et son template

### Vue json / DRF

- créer une vue liste en json
- installer DRF
- créer un serializer et son viewset

## Séance 1 : Django initiation (1 journée)

- durée : 3h30

### Pré-requis

- 

### Déroulé

### Post séance

- 

### Resources

- 

## Séance 2 : 

- Rappels
- Tests automatisés
- Projets / tester mon application


## Else

- Statics / Projets / personnaliser l'apparence avec des CSS et des images
- Traduction
- Fichiers
- Users
- Admin django avancé
- App django réutilisable
- Déployer son application django
- Django packages
- ORM / Django shell
- Django commandes (créer sa commande)
- Templates
