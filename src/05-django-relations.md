---
title: Django – Relations
author: Emmanuelle Helly
theme: solarized
---

# Relations entre les modèles

----

## Champs de relations

La bibliothèque ``django.models`` fournit différents champs spécifiques pour
représenter les relations entre modèles.

* ``models.ForeignKey`` : représente une relation de type __1-N__
* ``models.ManyToManyField`` : représente une relation de type __N-N__
* ``models.OneToOneField`` : représente une relation de type __1-1__

----

## Le champ ForeignKey

Pour un portion de nourriture par exemple :

```python
class FoodPortion(models.Model):
    """ Food 1:n FoodPortion"""
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    qty = models.CharField(max_length=30)
```

----

Accéder à l'aliment d'une portion

```python
>>> food_portion_object.food
```

Accéder aux portions d'un aliment

```python
>>> food_object.foodportion_set.all()
<QuerySet [<FoodPortion: FoodPortion object (1)>]>
```

Changer le nom par lequel on accède aux portions d'un aliment: ajouter au champs le paramètre `related_name='portions'`

----

## Le champ ManyToManyField

Le champ ``ManyToManyField`` doit être déclaré de la même manière que le champ
``ForeignKey``.

La représentation de ce champ en base de données est une table contenant deux
clés étrangères vers les deux tables des modèles liés.

----

### Exemple

Un aliment peut appartenir à plusieurs catégories, et une catégorie peut concerner plusieurs aliments.

```python
# books/models.py
class Category(models.Model):
    label = models.CharField(max_length=50)

class Food(models.Model):
    categories = models.ManyToManyField(Category)
```

Accéder aux catégories d'un aliment

```python
food_object.categories.all()
```

Accéder aux aliments d'une catégorie

```python
category_object.food_set.all()
```

----

## Le champ OneToOneField

La déclaration du ``OneToOneField`` est similaire.

La représentation de ce champ en base de données est une clé étrangère possédant une contrainte d'unicité.

----

### Exemple

Un profil est associé à un seul utilisateur et réciproquement

```python
# books/models.py
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User)
    zipcode = models.CharField(max_length=6)
```

Accéder à l'utilisateur depuis le profil

```python
profile_object.user
```

Accéder au profil depuis l'utilisateur

```python
user_object.profile
```

---

# ORM et performance

----

## Le problème N°1 avec les ForeignKey

On accède à une relation dans une boucle ce qui entraine :

* **1** requête pour récupérer la collection de taille N sur laquelle on boucle
* **N** requêtes pour récupérer l'attribut lié pour chaque objet

----

#### Exemple


```django
{% for task in object_list %}
<li>
    <a href="{% url 'task_detail' task.pk %}">{{ task }}</a>
    Liste: {{task.todo_list.label }}
</li>
{% endfor %}
```

----

## Diagnostic

Pour diagnostiquer ceci, on utilise le module `django-debug-toolbar`

![](img/ddt_nplus1.png)

Suivre la documentation d'installation :

<https://django-debug-toolbar.readthedocs.io/en/latest/installation.html>

----

## Solution pour les ForeignKey

**select_related()**

```python
class TaskList(ListView):
    model = Task

    def get_queryset(self):
        queryset = super(TaskList, self).get_queryset()
        return queryset.select_related("todo_list")
```

L'ORM fait une seule requête avec une jointure :

![](img/ddt_nplus1_fixed.png)

----

## Le problème N°1 avec les ManyToManyField

    {% for list in object_list %}
      <li>
        <a href="{% url 'todolist_detail' list.pk %}">{{ list }}</a>
        Users: {% for user in list.users.all %}
                  {{ user.username }}
               {% endfor %}
      </li>
    {% endfor %}

![](img/ddt_nplus1_manytomany.png)

----

## Solution pour les ManyToMany

**prefetch_related()**

```python
class TodoListList(ListView):
    model = TodoList

    def get_queryset(self):
        queryset = super(TodoListList, self).get_queryset()
        return queryset.prefetch_related("users")
```

L'ORM ne fait qu'une seule requête supplémentaire avec une clause ``IN`` :

![](img/ddt_nplus1_manytomany_fixed.png)
