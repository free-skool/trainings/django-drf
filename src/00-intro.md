---
title: Initiation à Django
author: Emmanuelle Helly
theme: solarized
---

# Django / Django REST Framework

## 4 jours

---

## Présentations

Emmanuelle Helly, développeuse web et formatrice

----

## Sommaire

### Django

- Installer django, créer une application
- Première vue et URLs
- Les modèles et relations
- Les outils de debug et les tests
- Les champs pour les modèles
- Django et les données géographiques
- Requêtes et réponses

----

### REST

- Méthodes HTTP
- Principes des APIs REST

----

### Django REST Framework

- Ajouter DRF au projet
- Première API
- Serializers
- Viewsets et Routers
- Documenter son API
- Relations
- Authentifications
- Versions API, pagination

----

## TP pour le tutoriel

Application de sondage

* Question
* Choice

---

# Introduction

----

## Qu'est-ce-que Django ?

* Framework en Python pour le Web
* Encourage le développement rapide et propre avec une conception pragmatique
* Django permet de construire des applications web rapidement et avec peu de code
* Malgré son haut niveau d'abstraction, il est toujours possible de descendre dans les couches

----

## Historique

* Créé en 2003 par le journal local de Lawrence (Kansas, USA), basé sur le langage Python créé en 1990
* Rendu Open Source (BSD) en 2005
* Django 2.x et 3.x : versions en cours (Django 1.11 bientôt obsolète)
* Aujourd'hui utilisé par de très nombreuses entreprises/sites : Mozilla, Instagram, Pinterest, Disqus, Washington Times, ...

----

## Un framework, pour quoi faire ?

- Le code commun d'une application web / base de donnée est déjà écrit  
    → ORM, sessions, requêtes et réponses, templating…
- Moins de possibilité de failles de sécurité
- Seul le code métier est nécessaire
- Permet de réaliser rapidement une application web métier

----

## La philo de Django

* Framework en python pour le web
* Philosophie _keep it simple, stupid_ et _don't reapeat yourself_
* Architecture _Model_, _Template_, _View_

----

### Couplage faible

* Les différentes couches du framework sont indépendantes
* Le socle d'applications peut être réduit au strict minimum

### Peu de code à écrire

* Ecriture "automatique" de code
* Utilisation des possibilités d'introspection de Django

----

## Architecture _Model_, _Template_, _View_ ?

S'inspire du principe MVC (*Model, View, Controller*)  
ou plutôt MTV (*Model, Template, View*) :

* __Model__ : définition des objets. Django fournit un ORM pour accéder à la base de données
* __Template__ : affichage des données
* __View__ : fonction ou classes retournant des réponses HTTP

L'_URL dispatcher_ : fait correspondre **URLs** et **vues**

----

## Les bonnes raisons d'utiliser Django

* Facile à installer
* Très complet
* Excellente documentation
* Modèles en Python et ORM faciles à utiliser
* Interface d'administration auto-générée
* Gestion de formulaires
* Serveur de développement inclus
* Extensible, nombreux modules existants
* Communauté autour du projet très active
