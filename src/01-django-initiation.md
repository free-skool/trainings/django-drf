---
title: Initiation à Django
author: Emmanuelle Helly
theme: solarized
---

# Initiation à Django

----

## Sommaire

- création projet + app
- Première vue et URLs
- Premier modèle
- Interface d'admin 
- Django en ligne de commande
- Deuxième modèle et relations
- Outils de debug
- Écrire un test
- (Bonus) Première vue en Json

---

# L'environnement de développement

----

## Versions utilisées

* Django 3.0+
* Python : 3.x
* Base de données : SQLite, PostgreSQL, MySQL

----

## Conventions de codage

La documentation précise certaines conventions de codage spécifiques à Django.

La PEP 8 fait référence pour le reste.
[https://docs.djangoproject.com/fr/2.0/internals/contributing/writing-code/coding-style/]()

----

## Côté python

Python parcourt sys.path pour chercher les modules à importer

* Par défaut ce path contient les répertoires systèmes tels que ``/usr/lib/python``,
``/usr/local/lib/python``, ``~/.local/lib/python`` ainsi que le répertoire courant en général
* Comme tout module python, il faut que Django soit accessible dans le path pour pouvoir l'utiliser

----

* __Virtualenv__ permet de créer un environnement python en isolation du système,
c'est la méthode préférable pour développer avec python
* Contrairement à `PHP`, il ne faut pas mettre le code python dans `/var/www/`. Vous pouvez le mettre dans `/home/Dev/` par exemple.

----

## Installer et activer un _Virtualenv_

```!console
$ sudo apt install python3-venv # si ce n'est pas déjà fait
$ python3 -m venv venv
```

## Installation de Django

```!console
$ source venv/bin/activate
(venv) $ pip install django
```

---

# Créer notre projet

----

## Créer le projet

```!console
(venv) $ django-admin startproject monprojet
```

Rem: n'appelez pas votre projet "django" ou "test", ce sont des mots utilisés par Django.

----

## Lancer le serveur

```!console
(venv) $ cd monprojet
(venv) $ ./manage.py runserver
```

----

### It works !

![](./img/00-run-server.png)

----

## Créer l'application

```!console
(venv) $ ./manage.py startapp polls
```

----

## Structure du projet

```!console
monprojet
├── db.sqlite3
├── manage.py
├── monprojet
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── polls
    ├── admin.py
    ├── apps.py
    ├── migrations
    ├── models.py
    ├── tests.py
    └── views.py
```
----

### Fichiers de l'application

* ``models.py`` : déclaration des modèles de l'application
* ``views.py`` : écriture des vues de l'application
* ``admin.py`` : comportement de l'application dans l'interface d'administration
* ``tests.py`` : Il. Faut. Tester.
* ``migrations``: modifications successives du schéma de la base de données

----

## Projet vs. Application


### Une application

Constitue une fonctionnalité (système de blog, application de sondage)

### Un projet

Contient les réglages et les applications pour un site Web particulier

**Un projet est une combinaison d'applications**

----

## Activer l'application

```!python
# settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    ...
    'polls.apps.PollsConfig'
]
```

---

# Ma première vue

----

## Créer une vue page d'accueil pour l'application Polls

`index` reçoit la requête en paramètre, et renvoie une réponse.

```!python
# polls/views.py
from django.http import HttpResponse

def index(request):
    return HttpResponse("Bienvenue sur l'application Sondages !")
```

----

## Ajouter une URL

Déclarer un chemin et le relier à la vue

```!python
# polls/urls.py (il n'existe pas, vous devrez le créer)
from django.urls import path
from polls import views

urlpatterns = [
    path('', views.index),
    # …
]
```

----

## Inclusion d'*URLconf*

La configuration des URLs du projet inclue celles de chaque application

```!python
# monprojet/urls.py
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
]
```

Note:

TODO: explications sur `include()`, `path()` cf tuto

---

# Mon premier modèle

----

## Générer la structure de la base de données

Pour le moment, nous gardons le connecteur de base de données par défaut : SQlite3.

```!console
$ ./manage.py migrate
```

_Remarque : par défaut Django utilise SQLite, en production PostgreSQL est plus indiqué._

----

## Le modèle Question et ses champs

* Question text → CharField
* Date de publication → DateField

[Documentation sur les champs](https://docs.djangoproject.com/fr/3.0/ref/models/fields/)

_todo : schéma de données ?_

----

## Déclarer le modèle

```!python
# polls/models.py
class Question(models.Model):
    """Model for Question"""
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
```

----

## Types de champs

* Texte `CharField`, `EmailField`…
* Nombres : `IntegerField`, `FloatField`, `DecimalField`…
* Booléens :  `BooleanField` et `NullBooleandField`
* Dates : `DateField`, `TimeField`, `DurationField`…
* Fichiers : `FileField`, `ImageField`…

----

## Options pour les champs

Propriétés communes pour les champs

* ``verbose_name``: label du champ
* ``null`` : valeur NULL autorisée ou non en base de données
* ``default`` : valeur par défaut pour une nouvelle instance
* `unique` ajoute une contrainte d'unicité
* `validators` permet d'ajouter des contraintes de validation au niveau du modèle
* …

_cf_ [documentation sur les champs de modèle](https://docs.djangoproject.com/fr/stable/ref/models/fields/#field-options)

----

## Migrations de la base de données

* Générer le fichier de migration

    ```!console
    $ ./manage.py makemigrations
    ```

* Appliquer les migrations à la base de données

    ```!console
    $ ./manage.py migrate
    ```

**Rem:** Commande pour visualiser les modifications apportées à la base de donnée

```!console
$ ./manage.py sqlmigrate polls 0001
```
---

# Les migrations

----

Django permet de faire évoluer les modèles sans effacer les données,
en génèrant des « diffs » appelés migrations, qu'il applique ensuite à la base de données

----

## Génération des migrations

`./manage.py makemigrations`

1. Compare la dernière migration aux modèles déclarés
2. Génère un nouveau fichier de migration

----

## Application des migrations

`./manage.py migrate`

1. Convertit en SQL la ou les migrations qui n'ont pas encore été appliquées
2. Exécute le code SQL sur la base de donnée

----

## Remarques

La liste des migrations déjà faites est stockée dans la table
`django_migrations` en base de donnée.

Les fichiers de migrations sont numérotés et rangés dans `migrations/`.

Le SQL généré dépend de la base de donnée utilisée.

_cf_ [documentation sur les migrations](https://docs.djangoproject.com/fr/stable/topics/migrations/)

---

# Le shell Django

----

## Manipuler les objets


```!console
$ ./manage.py shell
```

### Lister les objets d'un modèle

```!python
>>> from polls.models import Question
>>> Question.objects.all()
<QuerySet []>
```

----

### Créer un objet

```!python
>>> from django.utils import timezone
>>> q = Question(question_text="Comment ça va ?", pub_date=timezone.now())
>>> q
<Question: Question object (None)>
```

Sauvegarder l'objet dans la base de données

```!python
>>> q.save()
>>> q.id
1
>>> q
<Question: Question object (1)>
```
----

### Manipuler l'objet

```!python
>>> q.question_text = "Ça roule ?"
>>> q.save()
```

----

### Personnaliser la représentation de l'objet

```!python
class Question(models.Model):
    # ...
    def __str__(self):
        return self.question_text
```

```!python
>>> from polls.models import Question
>>> Question.objects.all()
<QuerySet [<Question: Ça roule ?>]>
```

Note:

Quitter le shell et s'y reconnecter pour que ça soit pris en compte.

---

# L'administration Django

----

## Créer un super utilisateur

```!console
$ ./manage.py createsuperuser
```

Puis se connecter sur <http://127.0.0.1:8000/admin/>

## Déclarer le modèle dans l'admin

```!python
### polls/admin.py
from django.contrib import admin
from polls.models import Question

admin.site.register(Question)
```
----

## Modèle dans l'interface admin

![](./img/03-admin.png)

----

## "back-office automatique" de Django

Liste les instances et par introspection des modèles, crée les formulaires de
création/modification correspondants.

Personnalisable, permet de modifier :

* les filtres et l'ordre des listes
* l'affichage des listes
* les formulaires et l'ordre des champs
* ajouter des actions en masse sur les listes

_cf_ [documentation sur l'interface d'administration](https://docs.djangoproject.com/fr/stable/ref/contrib/admin/)

---

# Relations entre les modèles

----

## Les champs de relations

Champs spécifiques pour représenter les relations entre modèles.

* `models.ForeignKey` : relation __1-N__
* `models.ManyToManyField` : relation __N-N__
* `models.OneToOneField` : relation __1-1__

Nous allons expérimenter le premier, et verrons les deux autres plus tard.

----

## Relation __1-N__ : `models.ForeignKey`

* Premier argument le modèle auquel il fait référence
* `related_name`: nomme la relation inverse
* En base de donnée : contrainte de type clé étrangère

Note:

Le champ `ForeignKey` doit être déclaré avec comme premier argument le modèle
auquel il est lié par cette relation 1-N. L'argument optionnel `related_name`
permet de nommer la relation inverse à partir de ce modèle lié.

La représentation de ce champ en base de données est une contrainte de type clé étrangère.

----

Par exemple : 1 question ←→ N choix.

Créer un deuxième modèle `Choice`

```!python
# polls/models.py
class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
```

---

# Et ensuite

* Django permet de réaliser une appli web complète
* Système de vues, templates, formulaires…
* Dans notre cours : API utilisable par une autre appli web

Note:

Il est possible de n'utliser que Django pour réaliser une application web de bout en bout. Des vues listant les objets ou bien des formulaires de saisie peuvent être créés.

Dans notre cours, nous allons nous concentrer sur la réalisation d'une API utilisable par une autre

TODO: Faire une vue liste et une vue détail
