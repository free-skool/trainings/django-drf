---
title: Django – Tests, requêtes et réponses
author: Emmanuelle Helly
theme: solarized
---

# Django 

## Tests, requêtes et réponses

----

## Sommaire

- Tests
- Vues
- Many to Manye

---

# Les types de tests

----

## Principaux types de test

### Test unitaire

- Tests sur chaque composant de façon indépendant.
- Faciles à automatiser
- python : unittest

----

### Test d'intégration

- Tests sur les liens entre les composants
- Assez faciles à automatiser
- Django tests

----

### Test système ou fonctionnels

- Tests sur les fonctionnalités
- Pas toujours facile à automatiser
- Django tests

----

### Tests "end to end"

- Scénario sur un parcours d'utilisation de bout en bout
- On vient de passer à l'ère Gutemberg en terme de tests "e2e" automatisés
- Cypress

----

### Autres types de tests

- Tests de performance
- Tests d'intrusion → sécurité
- Tests utilisateurs → pour les IHM
- "Monkey tests"

----

## Pourquoi les tests ?

- Garantir une application fiable
- Éviter les régressions au fil du temps
- Gérer les mises à jour sereinement
- Gagner du temps dans le développement

---

# Tests automatisés dans Django

----

## Les outils

- la classe TestCase de Django
- le client permettant de gérer les requêtes et les réponses

[Documentation sur les tests](https://docs.djangoproject.com/fr/3.0/topics/testing/), [Tutoriel](https://docs.djangoproject.com/fr/3.0/intro/tutorial05/)

----

## Ça ressemble à…

```bash
from django.test import TestCase
from myapp.models import Animal

class AnimalTestCase(TestCase):
    def setUp(self):
        Animal.objects.create(name="lion", sound="roar")
        Animal.objects.create(name="cat", sound="meow")

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        lion = Animal.objects.get(name="lion")
        cat = Animal.objects.get(name="cat")
        self.assertEqual(lion.speak(), 'The lion says "roar"')
        self.assertEqual(cat.speak(), 'The cat says "meow"')
```

----

## Lancer les tests

```bash
./manage.py test
```

Seulement pour un module

```bash
./manage.py test manimals
```

Ou une classe, ou une méthode

```bash
./manage.py test manimals.tests.AnimalTestCase.test_animals_can_speak
```
----

## Arborescence conseillée

Créer dans un dossier ``tests``, un fichier de tests
(``test_views.py``, ``test_models.py``, ...) par fichier de
l'application (``views.py``, ``models.py``, ...).

```plaintext
├── todolist
│   ├── __init__.py
│   ├── forms.py
│   ├── models.py
│   ├── views.py
│   ├── tests
│   │   ├── __init__.py
│   │   ├── test_forms.py
│   │   ├── test_models.py
│   │   └── test_views.py
```

---

# Mes premiers tests

----

## Exécution

Dans le dossier du projet

```nutritionle
$ ./manage.py test
System check identified no issues (0 silenced).

----------------------------------------------------------------------
Ran 0 tests in 0.000s

OK
```

----

## Premier test (qui plante)

```python
# nutrition/tests.py
from django.test import TestCase


class TestNutrition(TestCase):
    def test_truth(self):
        assert False
```

----

### Résultat


```nutritionle
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
F
======================================================================
FAIL: test_truth (nutrition.tests.TestNutrition)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/home/numahell/Dev/formation/django/django-nutricio/src/nutrition/tests.py", line 6, in test_truth
    assert False
AssertionError

----------------------------------------------------------------------
Ran 1 test in 0.001s

FAILED (failures=1)
Destroying test database for alias 'default'...
```

----

## Premier test (qui passe)


```python
from django.test import TestCase


class TestNutrition(TestCase):
    def test_truth(self):
        assert True
```

----

### Résultat

```nutritionle
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
.
----------------------------------------------------------------------
Ran 1 test in 0.001s

OK
Destroying test database for alias 'default'...
```

----

## Testons les URL

```python
from django.test import TestCase


class TestNutrition(TestCase):
    def test_resolve_home_url(self):
        response = self.client.get('/api/')
        self.assertEqual(response.status_code, 200)
```

----

### Configurer la nouvelle URL

```python
# src/projnutricio/urls.py

urlpatterns = [
    path('api/', include('nutrition.urls')),
]
```

### Ajoutons notre vue

La faire pointer sur quelque chose

```python
# src/nutrition/views.py
def api_root_view(request):
    pass
```

----

### Résultat

Le test ne passe pas

```python
ValueError: The view nutrition.views.api_root_view didn't return an HttpResponse object. It returned None instead.
```

----

## Testons une vue de type fonction

Ajout d'un test utilisant le [client de test](https://docs.djangoproject.com/fr/3.0/topics/testing/tools/#the-test-client) de Django :

```python
class TestNutrition(TestCase):
    # ...
    def test_get_api_root(self):
        response = self.client.get("/api/")
        self.assertContains(response, "Bienvenue dans l'API")
```

### Mettons la vue à jour

Django passe automatiquement une [HttpRequest](https://docs.djangoproject.com/fr/3.0/ref/request-response/#httprequest-objects) 
à notre vue.

```python
def api_root_view(request):
    return HttpResponse("Bienvenue dans l'API")

```

----

### Résultat

```console
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
..
----------------------------------------------------------------------
Ran 2 tests in 0.010s

OK
Destroying test database for alias 'default'...
```

Django s'attends à ce que notre vue renvoie une [HttpResponse](https://docs.djangoproject.com/fr/3.0/ref/request-response/#django.http.HttpResponse).

----

---

# Requête et réponses

----

## Une vue : reçoit une requête et renvoie une réponse

Une **vue** est une fonction qui prend un objet `HttpRequest` et renvoie un objet `HttpResponse`.

----

## Déroulement d'une requête HTTP

Quand Django reçoit une requête HTTP :

* Crée l'objet `HttpRequest` correspondant à la requête du client
* Cherche la fonction de vue associée à l'URL
* Appelle cette fonction en lui passant l'objet `HttpRequest` en paramètre
* Récupère un objet `HttpResponse` en retour de la fonction ou de la classe
* Répond au client

----

## L'objet `HttpRequest`

Permet d'accéder à de nombreux attributs tels que

* Le schéma (ex. `http` ou `https`), le domaine, et le chemin formant l'URL
* La méthode (ex. `GET`, `POST`, `PUT`, `DELETE`)
* Les headers HTTP (ex. `Content-Type`)
* Les paramètres et les fichiers uploadés
* Les cookies

----

Peut être lu comme un fichier/flux

* `request.read()`
* `request.readline()`
* `for line in request:`

_cf._ [documentation objet `HttpRequest`](https://docs.djangoproject.com/fr/stable/ref/request-response/#httprequest-objects)

----

## L'objet `HttpResponse`

Permet de régler de nombreux attributs tels que

* Le statut HTTP (ex. `200 OK`, `404 Not Found`)
* Le contenu de la réponse (ex. du code HTML, des données sérialisées en JSON)
* Les headers HTTP (ex. `Content-Type`)
* Les cookies

----

Peut être instancié directement avec le contenu comme paramètre

```python
response = HttpResponse("foobar")
```

Peut être écrit comme un flux

```python
request.write()
```

Est dérivé en sous-classes (ex. `HttpResponseRedirect`)

_cf._ [documentation objet `HttpResponse`](https://docs.djangoproject.com/fr/stable/ref/request-response/#httpresponse-objects)

----

## Tests / requêtes et réponses

Testez dans le shell Django

```python
>>> from django.test import Client
>>> c = Client()
>>> response = c.post('/login/', {'username': 'john', 'password': 'smith'})
>>> response.status_code
200
>>> response = c.get('/api/v1/')
>>> response.content
```

[Documentation sur le client](https://docs.djangoproject.com/fr/3.0/topics/testing/tools/#the-test-client)

---

# Tester notre API

----

## Tester la résolution d'une URL

```python
from django.core.urlresolvers import resolve
from nutrition import views


class TestNutrition(TestCase):
    def test_resolve_api_url(self):
        response = self.client.get('/api/v1/')
        self.assertEqual(response['Content-Type'], '')
```

Lançons ce test pour voir ce que nous obtenons

----

### Rajoutons des urls à tester

```python
class TestNutrition(TestCase):
    def test_resolve_api_url(self):
        [...]
        self.assertIn('users', response.json())
        self.assertIn('foods', response.json())
        self.assertIn('foodportions', response.json())
```

Ce test ne passe pas : nous devons maintenant ajouter un "End point"

---

# Tests et persistance des données

----

## Nouveau test

On crée des aliments, on teste que l'API renvoie ces éléments

```python
# nutrition/tests.py
from nutrition.models import Food

class NutritionModelTest(TestCase):
    # ...

    def test_get_food(self):
        Food.objects.create(shrt_desc="Salade César")
        Food.objects.create(shrt_desc="Lasagnes")

        response = self.client.get("/api/v1/foods/")

        self.assertContains(response, "Salade César")
        self.assertContains(response, "Lasagnes")
```

Ajoutons un pdb pour voir ce qui s'y passe

----

## A savoir

La classe `dango.test.TestCase` :
- reset la base de données à l'état initial à chaque fin de test
- possède l'attribut `client` qui permet de faire des requêtes et conserver
les cookies de session
    - `self.client.get()` et `self.client.post()`
    - `self.client.login()` et `self.client.force_login()`

----

- permet de créer des données de test pour toutes les méthodes en
surchargeant `setUp()` ou `setUpTestData()`
- permet de charger des fixtures (données initiales en début de test)
- fournit quantités d'[assertions](https://docs.djangoproject.com/en/3.0/topics/testing/tools/#assertions)
    - `self.assertEqual(1, 3 - 2)`
    - `self.assertContains(response, "Mon titre")`
